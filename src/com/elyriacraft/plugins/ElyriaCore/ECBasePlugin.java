package com.elyriacraft.plugins.ElyriaCore;

import com.elyriacraft.plugins.ElyriaCore.plugin.MainClassTemplate;
import com.elyriacraft.plugins.ElyriaCore.plugin.PluginLogger;
import com.elyriacraft.plugins.ElyriaCore.plugin.storage.StorageManagerTemplate;


public class ECBasePlugin extends MainClassTemplate{
	    

	public static MainClassTemplate instance;{
		instance = this;
	}
	public static ECBasePlugin getInstance() {
		return (ECBasePlugin)instance;
	}
	@Override
	public void onDisable(){
    	saveConfig();
	}
	/**
	 * gets if the plugin is in debug mode or not
	 * @return debug mode true/false
	 */
	public final boolean isDebug() {
		return this.getConfig().getBoolean("Debug");
	}
	    
	/**
	 * Gets the plugin version
	 * @return plugin version
	 */
	public final String getVersion() {
		return this.getDescription().getVersion();
	}
	
	/**
	 * Gets the log manager for logging purposes
	 * @return the log manager
	 */
	public final PluginLogger getLog() {
		return this.log;
	}
	    
	/**
	 * Returns logging prefix
	 * @return the prefix
	 */
	public static final String getPrefix() {
		return "ElyraCore";
	}

	/**
	 * gets the StorageManager for the custom config file(s)
	 * @return StorageManager
	 */
	public final StorageManagerTemplate getStorageManager() {
		return this.storageMan;
	}

}