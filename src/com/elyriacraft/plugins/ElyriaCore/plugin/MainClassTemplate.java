package com.elyriacraft.plugins.ElyriaCore.plugin;

import java.util.List;

import org.bukkit.plugin.java.JavaPlugin;

import com.elyriacraft.plugins.ElyriaCore.plugin.PluginLogger;
import com.elyriacraft.plugins.ElyriaCore.plugin.messages.MessageRetriever;
import com.elyriacraft.plugins.ElyriaCore.plugin.storage.StorageManagerTemplate;


public abstract class MainClassTemplate extends JavaPlugin {

	protected StorageManagerTemplate storageMan;
	protected PluginLogger log = new PluginLogger(this);
	protected MessageRetriever msgRet;

    protected static MainClassTemplate instance;
    
	/**
	 * This method is called every time the plugin is enabled.
	 * This template covers the enable message, copies the default configs (that you have to specify)
	 * and saves the config.
	 * @see org.bukkit.plugin.java.JavaPlugin
	 */
	@Override
	public void onEnable() {
		msgRet = new MessageRetriever(this, "messages");
		log.info(getName() + " v" + getVersion() + " by " + " enabled!");
		getConfig().options().copyDefaults(true);
		saveConfig();
	}
	/**
	 * This method is called every time the plugin is disabled.
	 * This template covers the disable message and saving the config - nothing more.
	 * @see org.bukkit.plugin.java.JavaPlugin
	 */
	@Override
	public void onDisable(){
		log.info(getName() + " v" + getVersion() + " by " + getAuthors() + " disabled!");
		saveConfig();
	}

	/**
 	* gets if the plugin is in debug mode or not
 	* @return debug mode true/false
 	*/
	public boolean isDebug() {
        return this.getConfig().getBoolean("Debug");
	}

	/**
 	* Gets the plugin version
 	* @return plugin version
 	*/
	public String getVersion() {
        return this.getDescription().getVersion();
	}

	/**
 	* Gets the plugin author
 	* @return plugin version
 	*/
	public List<String> getAuthors() {
        return this.getDescription().getAuthors();
	}

	/**
 	* Gets the log manager for logging purposes
 	* @return the log manager
 	*/
	public PluginLogger getLog() {
		return this.log;
	}

	/**
	 * Returns logging prefix
	 * @return the prefix
 	*/
	protected static String name;
	public static String getPrefix(){
		return name;
	}

	/**
 	* gets the StorageManager for the custom config file(s)
 	* @return StorageManager
 	*/
	public StorageManagerTemplate getStorageManager() {
		return this.storageMan;
	}
	public MessageRetriever getMessageHandler() {
		return null;
	}
}
