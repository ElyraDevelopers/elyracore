package com.elyriacraft.plugins.ElyriaCore.plugin.messages;

import com.elyriacraft.plugins.ElyriaCore.plugin.MainClassTemplate;
import com.elyriacraft.plugins.ElyriaCore.plugin.storage.CustomFileManager;

public final class MessageRetriever {

	@SuppressWarnings("unused")
	private MainClassTemplate plugin;
	private CustomFileManager messageFile;
	
	public MessageRetriever(MainClassTemplate plugin, String fileName) {
		this.plugin = plugin;
		messageFile = new CustomFileManager(plugin, fileName);
	}
	
	public final void reloadMessages() {
		messageFile.reloadConfig();
	}
	
	public final String[] getMessage(IMessagePath message) {
		String[] returnString;
		String path = message.getPrefix() + message.getMessagePath();
		
		if (message.isList()) {
			returnString = (String[]) messageFile.getConfig().getStringList(path).toArray();
		} else {
			returnString = new String[]{messageFile.getConfig().getString(path)};
		}
		
		return returnString;
	}
    public final CustomFileManager getCustomFileManager(){
    	return messageFile;
    }
}