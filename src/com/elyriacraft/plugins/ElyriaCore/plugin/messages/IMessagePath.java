package com.elyriacraft.plugins.ElyriaCore.plugin.messages;

public interface IMessagePath {

	String getPrefix();
	
	String getMessagePath();
	
	boolean isList();
}