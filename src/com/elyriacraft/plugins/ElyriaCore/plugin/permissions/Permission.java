package com.elyriacraft.plugins.ElyriaCore.plugin.permissions;

/**
 * Implement this in your {@code PermissionMannager} enum. create a constructor like follows:
 * <p>
 * {@code private boolean playerDefault;}
 * <br>
 * <code>private PermissionMannager(boolean playerDefault){<br>
 * this.playerDefault = playerDefault;<br>
 * }</code>
 * <p>
 * then make the method {@code get()} return this:
 * <p>
 * {@code "NameOfPlugin." + this.toString().toLowerCase().replaceAll("_",".");}
 * <p>
 * Make sure you dont forget the dot after the plugin name.
 * <p>
 * Now you make the {@code isDefault()} method return the variable {@code playerDefault}
 * <p>
 * and finally, you register your permissions. Theese are the constants, defined like follows.
 * <br>Lets use the example permission {@code elyriacore.admin.reload:}
 * <p>
 * {@code ADMIN_RELOAD(false);}
 * <p>
 * the "elyriacore" part was added earlier at the {@code get()} method, the {@code ADMIN_RELOAD} is converted to lowercase, and the "_" is replaced with a "."
 * @author topisani
 *
 */
public interface Permission {

	/**
	 * Gets the permission as a string.
	 *
	 * @return the string
	 */
	public String get();
	
	/**
	 * Checks if the permission is given to players by default.
	 *
	 * @return true, if is default
	 */
	public boolean isDefault();
}
