package com.elyriacraft.plugins.ElyriaCore.plugin.game;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.elyriacraft.plugins.ElyriaCore.plugin.MainClassTemplate;
import com.elyriacraft.plugins.ElyriaCore.plugin.permissions.PermissionTemplate;
import com.elyriacraft.plugins.ElyriaCore.utils.StringUtils;
import com.elyriacraft.plugins.ElyriaCore.utils.exceptions.InventoryFullException;

public abstract class EcPlayer {

    private OfflinePlayer player;
    
    /**
     * The type "EcPlayer" is used for getting and setting plugin specific 
     * things about a player.
     * @param player the OfflinePlayer to get as a EtPlayer
     * @param storageManager 
     */
    public EcPlayer(OfflinePlayer player){
    	this.player = player;
    }
    /**
     * get the EtPlayer as an OfflinePlayer
     * @return OfflinePlayer
     */
    public final OfflinePlayer getPlayer(){
        return this.player;
    }
    /**
     * gets the EcPlayers name
     * @return name of the EcPlayer
     */
    public final String getName(){
        return this.player.getName();
    }
    public final void giveItem(ItemStack item) throws InventoryFullException{
    	Inventory pinv = player.getPlayer().getInventory();
    	HashMap<Integer, ItemStack> map = pinv.addItem(item);
    	if(item.getAmount() <= map.size()) {
    		throw new InventoryFullException("None of " + item.getAmount() + " " + item.getType().name() + "s could be placed in the player " + getName() + "'s inventory!");
    	}
    	if(map.size() != 0) {
    		throw new InventoryFullException(map.size() + " of " + item.getAmount() + " " + item.getType().name() + "s couldn't be placed in the player " + getName() + "'s inventory!");
    	}
    }
}
