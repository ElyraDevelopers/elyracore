package com.elyriacraft.plugins.ElyriaCore.plugin;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;

public final class PluginLogger {

	

    private MainClassTemplate plugin;
    
    private Logger logger;
    
    public PluginLogger(MainClassTemplate plugin) {
            this.plugin = plugin;
            logger = Bukkit.getLogger();
    }
    
    public final void debug(String message) {
            if (plugin.isDebug()) {
            	logger.log(Level.INFO, "[" + plugin.getPrefix() + " DEBUG] " + message);
            }
    }
    
    public final void info(String message) {
    	logger.log(Level.INFO, "[" + plugin.getPrefix() + "] " + message);
    }
    
    public final void warning(String message) {
    	logger.log(Level.WARNING, "[" + plugin.getPrefix() + "] " + message);
    }
    
    public final void severe(String message) {
        logger.log(Level.SEVERE, "[" + plugin.getPrefix() + "] " + message);
    }

}
