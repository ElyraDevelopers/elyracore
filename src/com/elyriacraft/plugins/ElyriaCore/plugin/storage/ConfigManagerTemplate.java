package com.elyriacraft.plugins.ElyriaCore.plugin.storage;

/**
   Use this as a template to your configManager. Make an enum that implemets this interface.
   start by defining the following variables:
   <p>
   {@code private String string = null;}<br>
   {@code private Object defaultValue = null;}<br>
   {@code private MainClassTemplate plugin;}<br>
   {@code private FileConfiguration co = plugin.getConfig();}<br>
   <p>
   then you create the following constructor:
   <p>
   {@code private ConfigManager(String config, Object defaultValue)}{<br>
   {@code this.string = config;}<br>
   {@code this.defaultValue = defaultValue;}<br>
   }
   <p>
   then you create your get method. this method will return an object that you can cast to the needed type.
   <p>
   {@code @Override}<br>
   {@code public Object get()}{<br>
   {@code return co.get(string);}<br>
   }
   <p>
   next step is the set method. this method will be used for setting values to the option.
   <p>
   {@code @Override}<br>
   {@code public void set(Object value)}{<br>
   {@code co.set(string, value);}<br>
   {@code plugin.saveConfig();}<br>
   }
   <p>
   and the last method is the one to add the defaults. this one will add the default of the current option, but it wont override other options.
   <p>
   {@code @Override<br>
   {@code public void addDefault()}{<br>
   {@code co.addDefault(string, defaultValue);}<br>
   }
   <p>
   now the only thing left is to add the constants. theese will be in the following format, at the very top just after the {@code public enum ConfigManager implements ConfigMannagerTemplate}{ thing.:
   <p>
   {@code SOME_OPTION("path in config seperated as strings", default value as an Object),}
   <p>
   the last option will be ended with a ";" instead of a ","
   @author topisani
 
 */
public interface ConfigManagerTemplate {
	
	/**
	 * Get the option defined in the config.yml
	 * @return The option defined in the config.
	 * If not defined will return default value.
	 * If no default value is present will return null
	 */
	public Object get();
	/**
	 * set the value to the given object.
	 * @param value to set
	 */
	public void set(Object value);
	/**
	 * Adds the default specified in the Enumerator.
	 */
	public void addDefault();
	
	
}
