package com.elyriacraft.plugins.ElyriaCore.plugin.storage;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.elyriacraft.plugins.ElyriaCore.plugin.MainClassTemplate;

public final class CustomFileManager {

        private MainClassTemplate plugin;

        private String fileName;
        private String path;
        private File customFile;
        private FileConfiguration customConfig;

        public CustomFileManager(MainClassTemplate plugin, String fileName) {
                this(plugin, fileName, "");
        }

        public CustomFileManager(MainClassTemplate plugin, String fileName, String path) {
                this.plugin = plugin;
                this.fileName = fileName;
                this.path = path;
                this.reloadConfig();
        }
        
        /**
         * Reload file from disk
         */
        public final void reloadConfig() {
                plugin.getLog().debug("(" + fileName + ") reloadConfig");
                if (customFile == null) {
                        plugin.getLog().debug("(" + fileName + ") customFile == null, creating file");
                        if (path.length() > 1) {
                                customFile = new File(plugin.getDataFolder() + File.separator + path, fileName + ".yml");
                        } else {
                                customFile = new File(plugin.getDataFolder(), fileName + ".yml");
                        }
                        saveFile();
                }
                plugin.getLog().debug("(" + fileName + ") customFile name: " + customFile.getName());
                plugin.getLog().debug("(" + fileName + ") reloaded customFile, setting customConfig");
                customConfig = YamlConfiguration.loadConfiguration(customFile);
                plugin.getLog().debug("(" + fileName + ") customConfig is null: " + (customConfig == null));
        }

        /**
         * Return the config from file
         * @return
         */
        public final FileConfiguration getConfig() {
                if (customConfig == null) {
                        this.reloadConfig();
                }
                return customConfig;
        }

        /**
         * Save changes to file on disk
         */
        public final void saveFile() {
            plugin.getLog().debug("Saving file: " + this.fileName);
            if (customConfig == null || customFile == null) {
            	plugin.getLog().debug("(saveFile) for " + this.fileName + " customConfig is null: " + (customConfig == null));
                plugin.getLog().debug("(saveFile) for " + this.fileName + " customFile is null: " + (customFile == null));
                this.reloadConfig();
            } else {
                plugin.getLog().debug("Attempting to save to file");
                try {
                	this.getConfig().save(customFile);
                	plugin.getLog().debug("File saved successfully");
                } catch (IOException ex) {
                	plugin.getLog().severe("Could not save file: " + this.fileName);
                }
            }
        }

}
