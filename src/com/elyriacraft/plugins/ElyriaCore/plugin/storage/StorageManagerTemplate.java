package com.elyriacraft.plugins.ElyriaCore.plugin.storage;

import com.elyriacraft.plugins.ElyriaCore.plugin.MainClassTemplate;

/**
 * The template for the StorageManager. all you need to do is to define the files you need this way: <p>
 * {@code private CustomFileManager PlayerFile = new CustomFileManager(plugin, "players";}
 * <p> add that just after the constructor. you can initialise it in the constructor itself for better security.
 * then you add a method to get that file: <p>
 * {@code public CustomFileManager getPlayerFile()}{<br>
 * {@code return this.playerFile;}<br>
 * }<p>
 * and finally you make sure the {@code reloadAll()} method also reloads the file you created:
 * <p>{@code @Override}
 * <br>{@code public void reloadAll()}{
 * <br>{@code playerFile.reload();}
 * <br>}
 * <p>in that way you can have as many custom configs as you want. just make sure to:
 * <p>define them all as private {@code ConfigManagers}
 * <br>make a get method for all of them
 * <br>make sure they all reload in the overriden {@code reloadAll()} method.
 * <p>and there you go, your own {@code StorageManager}
 * @author topisani
 *
 */
public abstract class StorageManagerTemplate {

    protected MainClassTemplate plugin;
    public StorageManagerTemplate(MainClassTemplate plugin) {
            this.plugin = plugin;
    }
	public MainClassTemplate getPlugin(){
    	return this.plugin;
    }
    /**
     * reloads all custom configs
     */
    public abstract void reloadAll();
}





