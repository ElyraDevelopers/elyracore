package com.elyriacraft.plugins.ElyriaCore.utils.exceptions;

/**
 * @author Stealth2800
 */
public final class NameMatchesNoPlayersException extends Exception {

	private static final long serialVersionUID = 3963701676916006789L;

	public NameMatchesNoPlayersException() {
		super();
	}
	
	public NameMatchesNoPlayersException(String message) {
		super(message);
	}
}