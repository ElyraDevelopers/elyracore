package com.elyriacraft.plugins.ElyriaCore.utils.exceptions;

public class InventoryFullException extends Exception{

	private static final long serialVersionUID = 3448654300250957728L;

	public InventoryFullException(){
		super();
	}
	public InventoryFullException(String message){
		super(message);
	}
}
