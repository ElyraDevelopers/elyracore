package com.elyriacraft.plugins.ElyriaCore.utils.exceptions;

/**
 * @author Stealth2800
 */
public final class NameMatchesMultiplePlayersException extends Exception {
	
	private static final long serialVersionUID = 1345919061769688494L;
	public NameMatchesMultiplePlayersException() {
		super();
	}
	
	public NameMatchesMultiplePlayersException(String message) {
		super(message);
	}

}