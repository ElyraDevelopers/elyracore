package com.elyriacraft.plugins.ElyriaCore.utils.exceptions;
/**
 * @author topisani
 */
public final class PlayerNotOnlineException extends Exception{

	private static final long serialVersionUID = 5163164507997195371L;

	public PlayerNotOnlineException(){		
		super();
	}
	public PlayerNotOnlineException(String message){		
		super(message);
	}
	
}
