package com.elyriacraft.plugins.ElyriaCore.utils;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
 
// TODO: Auto-generated Javadoc
/**
 * <h1>Usage</h1> <code>
<pre style='color:#d1d1d1;background:#000000;'><html><body style='color:#d1d1d1; background:#000000; '><pre>
IconMenu menu = new IconMenu("My Fancy Menu", 9, new IconMenu.OptionClickEventHandler() <span style='color:#b060b0; '>{</span>
            <span style='color:#d2cd86; '>@</span>Override
            <span style='color:#e66170; font-weight:bold; '>public</span> <span style='color:#bb7977; '>void</span> onOptionClick<span style='color:#d2cd86; '>(</span>IconMenu<span style='color:#d2cd86; '>.</span>OptionClickEvent event<span style='color:#d2cd86; '>)</span> <span style='color:#b060b0; '>{</span>
                event<span style='color:#d2cd86; '>.</span>getPlayer<span style='color:#d2cd86; '>(</span><span style='color:#d2cd86; '>)</span><span style='color:#d2cd86; '>.</span>sendMessage<span style='color:#d2cd86; '>(</span><span style='color:#00c4c4; '>"You have chosen "</span> <span style='color:#d2cd86; '>+</span> event<span style='color:#d2cd86; '>.</span>getName<span style='color:#d2cd86; '>(</span><span style='color:#d2cd86; '>)</span><span style='color:#d2cd86; '>)</span><span style='color:#b060b0; '>;</span>
                event<span style='color:#d2cd86; '>.</span>setWillClose<span style='color:#d2cd86; '>(</span><span style='color:#e66170; font-weight:bold; '>true</span><span style='color:#d2cd86; '>)</span><span style='color:#b060b0; '>;</span>
            <span style='color:#b060b0; '>}</span>
        <span style='color:#b060b0; '>}</span>, plugin)
        .setOption(3, new ItemStack(Material.APPLE, 1), "Food", "The food is delicious")
        .setOption(4, new ItemStack(Material.IRON_SWORD, 1), "Weapon", "Weapons are for awesome people")
        .setOption(5, new ItemStack(Material.EMERALD, 1), "Money", "Money brings happiness")<span style='color:#d2cd86; '>;</span>
</pre>
 * @author nisovin
 * 
 */
public class IconMenu implements Listener {
 
    /** The name of the inventory. */
    private String name;
    
    /** The size of the inventory.. */
    private int size;
    
    /** The option click event handler. */
    private OptionClickEventHandler handler;
    
    /** The plugin. */
    private Plugin plugin;
   
    /** The option names. */
    private String[] optionNames;
    
    /** The option icons. */
    private ItemStack[] optionIcons;
   
    /**
     * Instantiates a new icon menu.
     *
     * @param name the name of the inventory.
     * @param size the size of the inventory.
     * @param handler the option click event handler.
     * @param plugin the plugin
     */
    public IconMenu(String name, int size, OptionClickEventHandler handler, Plugin plugin) {
        this.name = name;
        this.size = size;
        this.handler = handler;
        this.plugin = plugin;
        this.optionNames = new String[size];
        this.optionIcons = new ItemStack[size];
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }
   
    /**
     * Sets the option.
     *
     * @param position the position
     * @param icon the icon
     * @param name the name
     * @param info the lore
     * @return the icon menu
     */
    public IconMenu setOption(int position, ItemStack icon, String name, String... info) {
        optionNames[position] = name;
        optionIcons[position] = setItemNameAndLore(icon, name, info);
        return this;
    }
   
    /**
     * Open the inventory for the player.
     *
     * @param player the player
     */
    public void open(Player player) {
        Inventory inventory = Bukkit.createInventory(player, size, name);
        for (int i = 0; i < optionIcons.length; i++) {
            if (optionIcons[i] != null) {
                inventory.setItem(i, optionIcons[i]);
            }
        }
        player.openInventory(inventory);
    }
   
    /**
     * Destroy.
     */
    public void destroy() {
        HandlerList.unregisterAll(this);
        handler = null;
        plugin = null;
        optionNames = null;
        optionIcons = null;
    }
   
    /**
     * On inventory click.
     *
     * @param event the event
     */
    @EventHandler(priority=EventPriority.MONITOR)
    void onInventoryClick(InventoryClickEvent event) {
        if (event.getInventory().getTitle().equals(name)) {
            event.setCancelled(true);
            int slot = event.getRawSlot();
            if (slot >= 0 && slot < size && optionNames[slot] != null) {
                Plugin plugin = this.plugin;
                OptionClickEvent e = new OptionClickEvent((Player)event.getWhoClicked(), slot, optionNames[slot]);
                handler.onOptionClick(e);
                if (e.willClose()) {
                    final Player p = (Player)event.getWhoClicked();
                    Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                        public void run() {
                            p.closeInventory();
                        }
                    }, 1);
                }
                if (e.willDestroy()) {
                    destroy();
                }
            }
        }
    }
    
    /**
     * The Interface OptionClickEventHandler.
     */
    public interface OptionClickEventHandler {
        
        /**
         * On option click.
         *
         * @param event the event
         */
        public void onOptionClick(OptionClickEvent event);       
    }
    
    /**
     * The Class OptionClickEvent.
     */
    public class OptionClickEvent {
        
        /** The player. */
        private Player player;
        
        /** The position. */
        private int position;
        
        /** The name. */
        private String name;
        
        /** The close. */
        private boolean close;
        
        /** The destroy. */
        private boolean destroy;
       
        /**
         * Instantiates a new option click event.
         *
         * @param player the player
         * @param position the position
         * @param name the name
         */
        public OptionClickEvent(Player player, int position, String name) {
            this.player = player;
            this.position = position;
            this.name = name;
            this.close = true;
            this.destroy = false;
        }
       
        /**
         * Gets the player.
         *
         * @return the player
         */
        public Player getPlayer() {
            return player;
        }
       
        /**
         * Gets the position.
         *
         * @return the position
         */
        public int getPosition() {
            return position;
        }
       
        /**
         * Gets the name.
         *
         * @return the name
         */
        public String getName() {
            return name;
        }
       
        /**
         * Will close.
         *
         * @return true, if successful
         */
        public boolean willClose() {
            return close;
        }
       
        /**
         * Will destroy.
         *
         * @return true, if successful
         */
        public boolean willDestroy() {
            return destroy;
        }
       
        /**
         * Sets the will close.
         *
         * @param close the new will close
         */
        public void setWillClose(boolean close) {
            this.close = close;
        }
       
        /**
         * Sets the will destroy.
         *
         * @param destroy the new will destroy
         */
        public void setWillDestroy(boolean destroy) {
            this.destroy = destroy;
        }
    }
   
    /**
     * Sets the item name and lore.
     *
     * @param item the item
     * @param name the name
     * @param lore the lore
     * @return the item stack
     */
    private ItemStack setItemNameAndLore(ItemStack item, String name, String[] lore) {
        ItemMeta im = item.getItemMeta();
            im.setDisplayName(name);
            im.setLore(Arrays.asList(lore));
        item.setItemMeta(im);
        return item;
    }
   
}