package com.elyriacraft.plugins.ElyriaCore.utils;

import org.bukkit.ChatColor;

public final class StringUtils {
	
	public static final boolean equalsIgnoreCaseMultiple(String main, String... equalities){
		for(String cur : equalities){
			if(main.equalsIgnoreCase(cur)){
				return true;
			}
		}
		return false;
	}
	public static final String makeInvisible(String string){
		char[] chars = string.toCharArray();
		String result = "";
		for(char cur : chars){
			String cChar = String.valueOf(ChatColor.COLOR_CHAR);
			String invisible = cChar + String.valueOf(cur);
			result.concat(invisible);
		}
		return result;
	}
	public static final String stringArraytoString(String[] strings) {
		String s = "";
		for(int i = 0; strings.length < i; i++) {
			String cur = strings[i];
			if(i == 0) {
				s = cur;
				continue;
			}
			if(i == (strings.length + 1)) {
				s.concat(" and " + cur);
				break;
			}
			s.concat(", " + cur);
		}
		return s;
	}
	
}
