package com.elyriacraft.plugins.ElyriaCore.utils;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;

public class ChunkUtils {

	public static String toString(Chunk c){
		return c.getWorld() + "," + c.getX() + "," + c.getZ();
	}
	public static Chunk fromString(String s){
		String[] ss = s.split(",");
		Chunk c = Bukkit.getServer().getWorld(ss[0]).getChunkAt(Integer.parseInt(ss[1]), Integer.parseInt(ss[2]));
		return c;
	}
}
