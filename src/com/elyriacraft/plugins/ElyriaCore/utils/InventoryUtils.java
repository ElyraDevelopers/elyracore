package com.elyriacraft.plugins.ElyriaCore.utils;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.json.JSONException;
import org.json.JSONObject;


public final class InventoryUtils {

	public static void addItemToInv(Player player, Material item, int amount){
		ItemStack itemstack = new ItemStack(item.getId());
		addItemToInv(player, itemstack, amount);
	}
	public static void addItemToInv(Player player, ItemStack item, int amount){
		PlayerInventory inv = player.getInventory();
		inv.addItem(item);
	}
    public static List<String> toString(Inventory inv) {
        List<String> result = new ArrayList<String>();
        List<ConfigurationSerializable> items = new ArrayList<ConfigurationSerializable>();
        for(ItemStack is:inv.getContents()) {
            items.add(is);
        }
        for(ConfigurationSerializable cs:items) {
            if(cs == null) {
                result.add("null");
            }
            else {
                result.add(new JSONObject(InventorySerializer.serialize(cs)).toString());
            }
        }
        return result;
    }

    public static Inventory toInventory(List<String> stringItems, String title) {
        Inventory inv = Bukkit.createInventory(null, 54, title);
        List<ItemStack> contents = new ArrayList<ItemStack>();
        for(String piece:stringItems) {
            if(piece.equalsIgnoreCase("null")) {
                contents.add(null);
            }
            else {
                try {
                    ItemStack item = (ItemStack) InventorySerializer.deserialize(InventorySerializer.toMap(new JSONObject(piece)));
                    contents.add(item);
                } catch(JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        ItemStack[] items = new ItemStack[contents.size()];
        for(int x = 0; x < contents.size(); x++)
            items[x] = contents.get(x);
        inv.setContents(items);
        return inv;
    }
    
}