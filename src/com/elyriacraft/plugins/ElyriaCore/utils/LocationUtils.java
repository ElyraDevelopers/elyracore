package com.elyriacraft.plugins.ElyriaCore.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public final class LocationUtils {

	public static final String toString(Location loc){
		return loc.toString();
	}
	public static final String toBlockString(Location loc){
		loc.setPitch(0);
		loc.setYaw(0);
		loc.setX(Location.locToBlock(loc.getX()));
		loc.setY(Location.locToBlock(loc.getY()));
		loc.setZ(Location.locToBlock(loc.getZ()));
		return loc.toString();
	}
	public static final Location fromString(String string){
		Location loc = Bukkit.getWorlds().get(1).getSpawnLocation();
		if(string.startsWith("Location{")){
			string.replaceFirst("Location{", "");
			String[] strings = string.split(",");
			for(String cur : strings){
				if(cur.startsWith("world=")){
					cur = cur.replaceFirst("world=", "");
					loc.setWorld(Bukkit.getWorld(cur));
				}
				if(cur.startsWith("x=")){
					cur = cur.replaceFirst("x=", "");
					loc.setX(Double.parseDouble(cur));
				}
				if(cur.startsWith("y=")){
					cur = cur.replaceFirst("y=", "");
					loc.setY(Double.parseDouble(cur));
				}
				if(cur.startsWith("z=")){
					cur = cur.replaceFirst("z=", "");
					loc.setZ(Double.parseDouble(cur));
				}
				if(cur.startsWith("pitch=")){
					cur = cur.replaceFirst("pitch=", "");
					loc.setPitch(Float.parseFloat(cur));
				}
				if(cur.startsWith("yaw=")){
					cur = cur.replaceFirst("yaw=", "").replaceFirst("}", "");
					loc.setYaw(Float.parseFloat(cur));
				}
			}
		}
		return loc;
	}
}
