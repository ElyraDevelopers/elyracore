/**
 * 
 */
package com.elyriacraft.plugins.ElyriaCore.utils;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.elyriacraft.plugins.ElyriaCore.utils.exceptions.NameMatchesMultiplePlayersException;
import com.elyriacraft.plugins.ElyriaCore.utils.exceptions.NameMatchesNoPlayersException;
import com.elyriacraft.plugins.ElyriaCore.utils.exceptions.PlayerNotOnlineException;

/**
 * @author topisani 
 * @author Stealth2800
 */
public final class PlayerUtils {
	
	public final static Player getPlayer(String name) throws NameMatchesNoPlayersException, NameMatchesMultiplePlayersException {
		List<Player> matchedPlayers = Bukkit.matchPlayer(name);
		
		if (matchedPlayers.size() == 0) {
			throw new NameMatchesNoPlayersException("Player '" + name + "' doesn't match any online players!");
		} else if (matchedPlayers.size() > 1) {
			throw new NameMatchesMultiplePlayersException("Player '" + name + "' matches multiple online players!");
		} else {
			return Bukkit.getPlayer(name);
		}
	}
	public final static Player getOnlinePlayer(String name) throws PlayerNotOnlineException{
		if(!Bukkit.getPlayer(name).isOnline()){
			throw new PlayerNotOnlineException("Player '" + name + "' doesnt seem to be online!");
		} else {
			return Bukkit.getPlayer(name);
		}
	}
	
}
