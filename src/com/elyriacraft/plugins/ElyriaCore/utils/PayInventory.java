package com.elyriacraft.plugins.ElyriaCore.utils;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.elyriacraft.plugins.ElyriaCore.plugin.MainClassTemplate;
/**
 * <h1>Usage:</h1>
 * 
<pre style='color:#d1d1d1;background:#000000;'><html><body style='color:#d1d1d1; background:#000000; '><pre>
PayInventory pay = new PayInventory(plugin, "InventoryName", 9, 500, topisani, new PayInventory.BoughtItemHandler() <span style='color:#b060b0; '>{</span>
            <span style='color:#d2cd86; '>@</span>Override
            <span style='color:#e66170; font-weight:bold; '>public</span> <span style='color:#bb7977; '>void</span> recieve<span style='color:#d2cd86; '>(</span>Player player<span style='color:#d2cd86; '>)</span> <span style='color:#b060b0; '>{</span>
                <span style='color:#9999a9; '>//this is what happens when the player pays</span>
                player<span style='color:#d2cd86; '>.</span>getInventory<span style='color:#d2cd86; '>(</span><span style='color:#d2cd86; '>)</span><span style='color:#d2cd86; '>.</span>addItem<span style='color:#d2cd86; '>(</span><span style='color:#e66170; font-weight:bold; '>new</span> ItemStack<span style='color:#d2cd86; '>(</span>Material<span style='color:#d2cd86; '>.</span>DIAMOND<span style='color:#d2cd86; '>,</span> <span style='color:#008c00; '>64</span><span style='color:#d2cd86; '>)</span><span style='color:#b060b0; '>;</span>
            <span style='color:#ffffff; background:#dd0000; '>}</span>
            <span style='color:#d2cd86; '>@</span>Override
            public <span style='color:#bb7977; '>void</span> notPayed<span style='color:#d2cd86; '>(</span>Player player<span style='color:#d2cd86; '>)</span> <span style='color:#b060b0; '>{</span>
                <span style='color:#9999a9; '>//this is what happens when the player didnt pay enough</span>
                player<span style='color:#d2cd86; '>.</span>sendMessage<span style='color:#d2cd86; '>(</span><span style='color:#00c4c4; '>"You didnt pay enough gold"</span><span style='color:#d2cd86; '>)</span><span style='color:#b060b0; '>;</span>
            <span style='color:#b060b0; '>}</span>
        <span style='color:#ffffff; background:#dd0000; '>}</span><span style='color:#d2cd86; '>)</span><span style='color:#b060b0; '>;</span>
</pre>
 * @author topisani
 *
 */
public class PayInventory implements Listener{

	private String name;
	private int size;
	private String title;
	private int cost;
	@SuppressWarnings("unused")
	private String playername;
	private BoughtItemHandler handler;
	   
	public PayInventory(MainClassTemplate plugin, String name, int size, int cost, String playername, BoughtItemHandler handler) {
		this.name = name;
		this.size = size;
		this.cost = cost;
		this.playername = playername;
		this.handler = handler;
		title = StringUtils.makeInvisible("{PAY|" + cost + "|" + playername + "}") + name;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	    
	public void open(Player player) {
        Inventory inventory = Bukkit.createInventory(player, size, name);
        player.openInventory(inventory);
    }
   
    public void destroy() {
        HandlerList.unregisterAll(this);
    }
   
    public interface BoughtItemHandler {
    	public void recieve(Player player);
    	public void notPayed(Player player);
    }
    
    @EventHandler(priority=EventPriority.MONITOR)
    public void onInventoryClose(InventoryCloseEvent event) {
        if (event.getInventory().getTitle().equals(title)) {
        	Inventory inv = event.getInventory();
        	ItemStack[] items = inv.getContents();
            int nuggets = 0;
            int ingots = 0;
            int money = 0;
            for (ItemStack item : items) {
                if ((item != null) && (item.getType() == Material.GOLD_NUGGET) && (item.getAmount() > 0)) {
                    nuggets += item.getAmount();
                }
                if ((item != null) && (item.getType() == Material.GOLD_INGOT) && (item.getAmount() > 0)) {
                    ingots += item.getAmount();
                }
            }
            money = ingots * 9 + nuggets;
            if(money >= cost) {
            	handler.recieve((Player)event.getPlayer());
            	int overflow = cost - money;
            	ItemStack item = new ItemStack(Material.GOLD_NUGGET, overflow);
            	event.getPlayer().getLocation().getWorld().dropItem(event.getPlayer().getLocation(), item);
            } else {
            	for(ItemStack item : items) {
            		event.getPlayer().getLocation().getWorld().dropItem(event.getPlayer().getLocation(), item);
            	}
            	handler.notPayed((Player)event.getPlayer());
            }
            destroy();
        }
    }
}

    
  